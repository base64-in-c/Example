#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
 
char* base64_encode(const unsigned char* data, size_t input_length) {
    BIO *bio, *b64;
    FILE* stream;
    size_t output_length = 4 * ((input_length + 2) / 3);
 
    char *encoded_data = (char *)malloc(output_length);
    if (encoded_data == NULL) return NULL;
 
    stream = fmemopen(encoded_data, output_length, "w");
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_fp(stream, BIO_NOCLOSE);
    bio = BIO_push(b64, bio);
 
    BIO_write(bio, data, input_length);
    BIO_flush(bio);
    BIO_free_all(bio);
 
    fclose(stream);
    return encoded_data;
}
 
int main() {
    const char* original_text = "B64Encode.com";
    size_t original_length = strlen(original_text);
 
    char* encoded_text = base64_encode((const unsigned char*)original_text, original_length);
    printf("Original Text: %s\nEncoded Text: %s\n", original_text, encoded_text);
 
    free(encoded_text);
    return 0;
}